﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace PreTest2
{
    class Program
    {
        const int ARRAYSIZE = 3; //3
        const int MINSCORE = 0;
        const int MAXSCORE = 100;

        static string[] firstNameArray = new string[ARRAYSIZE];
        static string[] lastNameArray = new string[ARRAYSIZE];
        static int[] testScoreArray = new int[ARRAYSIZE];


        static void Main(string[] args)
        {

            bool runProgramAgain = true;
            string doAgain = "";

            while (runProgramAgain)
            {
                Console.Clear();

                //fill each array with 10 values
                fillEachArray();

                determineLowTestScore();

                determineHighTestScore();

                determineAvgTestScore();

                Write("\nRun program again? (Y/N");
                doAgain = ReadLine().ToUpper();

                if (doAgain[0] != 'Y')
                {
                    runProgramAgain = false;
                    WriteLine("\n\nThaks for using the program!");
                }
            }
        }

        //input 10 first names, 10 last names, and 10 test scores
        static void fillEachArray()
        {
            string str = "";
            int score = 0;
            bool flag = false;

            for (int lcv = 0; lcv < firstNameArray.Length; ++lcv)
            {
                Write("\nEnter first name " + (lcv + 1).ToString() + ":");
                str = ReadLine();

                while (str.Trim() == "")
                {
                    WriteLine("First name CANNOT be empty!");
                    Write("\nEnter first name " + (lcv + 1).ToString() + ":");
                }

                firstNameArray[lcv] = str;

                Write("\nEnter last name " + (lcv + 1).ToString() + ":");
                str = ReadLine();

                while (str.Trim() == "")
                {
                    WriteLine("Last name CANNOT be empty!");
                    Write("\nEnter last name " + (lcv + 1).ToString() + ":");
                }

                lastNameArray[lcv] = str;

                flag = false;
                while (!flag)
                {
                    Write("please enter test score for student " + (lcv + 1).ToString() + ":");
                    str = ReadLine();

                    flag = (isNumeric(str));

                    if (flag)
                    {
                        score = Convert.ToInt32(str);
                        if ((score < MINSCORE) || (score > MAXSCORE))
                        {
                            WriteLine("OOR. Re-enter");
                            flag = false;
                        }
                        else
                        {
                            testScoreArray[lcv] = score;
                            flag = true;
                        }
                    }
                    else
                    {
                        WriteLine("Non-Numeric. Re-enter");
                        flag = false;
                    }
                }
            }
        }

        static private bool isNumeric(String input)
        {
            int test;
            return int.TryParse(input, out test);
        }

        static void showCurrentArrayContents()
        {
            for (int lcv = 0; lcv < firstNameArray.Length; ++lcv)
            {
                WriteLine("\nFirst Name Student " + (lcv + 1) + " is: " + firstNameArray[lcv]);
                WriteLine("\nLast Name Student " + (lcv + 1) + " is: " + lastNameArray[lcv]);
                WriteLine("\nTest Score Student " + (lcv + 1) + " is: " + testScoreArray[lcv]);
            }
        }

        static void determineLowTestScore()
        {
            int lowTestScore = 101;
            string theLows = "";

            for (int lcv = 0; lcv < testScoreArray.Length; ++lcv)
            {
                if (testScoreArray[lcv] <= lowTestScore)
                {
                    lowTestScore = testScoreArray[lcv];
                }
            }

            for (int lcv = 0; lcv < testScoreArray.Length; ++lcv)
            {
                if (testScoreArray[lcv] == lowTestScore)
                {
                    theLows += firstNameArray[lcv] + " " + lastNameArray[lcv] + "\n";
                }
            }

            WriteLine("\nLows Scores From:");
            WriteLine(theLows);
            WriteLine("Low Test Score: " + lowTestScore.ToString());
        }

        static void determineHighTestScore()
        {
            int highTestScore = -1;
            string theHighs = "";

            for (int lcv = 0; lcv < testScoreArray.Length; ++lcv)
            {
                if (testScoreArray[lcv] >= highTestScore)
                {
                    highTestScore = testScoreArray[lcv];
                }
            }

            for (int lcv = 0; lcv < testScoreArray.Length; ++lcv)
            {
                if (testScoreArray[lcv] == highTestScore)
                {
                    theHighs += firstNameArray[lcv] + " " + lastNameArray[lcv] + "\n";
                }
            }

            WriteLine("\nHighs Scores From:");
            WriteLine(theHighs);
            WriteLine("High Test Score: " + highTestScore.ToString());
        }

        static void determineAvgTestScore()
        {
            int totalPoints = 0;
            double testAvg = 0.0;

            for (int lcv = 0; lcv < testScoreArray.Length; ++lcv)
            {
                totalPoints += testScoreArray[lcv];
            }

            testAvg = (double)totalPoints / testScoreArray.Length;
            WriteLine("\nThe class test score average was: " + testAvg.ToString("f2"));
        }

    }
}
