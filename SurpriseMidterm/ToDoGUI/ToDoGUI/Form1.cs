﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Console;

namespace ToDoGUI
{
    public partial class ToDoGUI : Form
    {
        public ToDoGUI()
        {
            InitializeComponent();
        }

        const int ARRAYSIZE = 50;
        string[] importantTasks = new string[ARRAYSIZE];
        string[] nonImportantTasks = new string[ARRAYSIZE];
        string[] allTasks = new string[ARRAYSIZE];
        string[] dueDates = new string[ARRAYSIZE];

        string impTasks = "";
        string nonimpTasks = "";
        string dates = "";

        private void addNewTaskMenuItem_Click(object sender, EventArgs e)
        {
            bool importantTask = false;

            if (textBoxCurrentTask.Text == "")
            {
                alertMessage("No Item Created!",
                            "TEXTBOX EMPTY");
            }
            else
            {
                addTaskToListbox();

                importantTask = checkImportantCheckBoxStatus();

                if (importantTask)
                {
                    textBoxCurrentTask.Text += impTasks;
                    textBoxCurrentTask.Text += " - " + dateTimePickerDueDate.Value + "\n";

                    MessageBox.Show("Important Date " + impTasks,
                                    "IMPORTANT",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);

                }
                else
                {
                    textBoxCurrentTask.Text += nonimpTasks;
                    textBoxCurrentTask.Text += " - " + dateTimePickerDueDate.Value + "\n";

                    MessageBox.Show("Nonimportant Date " + impTasks,
                                    "NONIMPORTANT",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);

                }


            }
        }

        private void alertMessage(string text, string title)
        {
            MessageBox.Show(text, title,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
            textBoxCurrentTask.Focus();
        }

        private void addTaskToListbox()
        {
            listBoxAllTasks.Items.Add(textBoxCurrentTask.Text);
            textBoxCurrentTask.Text = "";
            textBoxCurrentTask.Focus();
        }

        private bool checkImportantCheckBoxStatus()
        {

            if (checkBoxImportant.Checked)
            {
                return true;
            }

            return false;
        }

        private void removeSingleTaskMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void removeAllTasksMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Remove all tasks
        }
    }
    
}
