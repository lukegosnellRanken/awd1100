﻿namespace ToDoGUI
{
    partial class ToDoGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addNewTaskMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showTasksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeTasksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimePickerDueDate = new System.Windows.Forms.DateTimePicker();
            this.labelDueDate = new System.Windows.Forms.Label();
            this.textBoxCurrentTask = new System.Windows.Forms.TextBox();
            this.labelCurrentTask = new System.Windows.Forms.Label();
            this.removeSingleTaskMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeAllTasksMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showImportantTasksMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showNonImportantTasksMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllTasksMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listBoxAllTasks = new System.Windows.Forms.ListBox();
            this.checkBoxImportant = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(435, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewTaskMenuItem,
            this.showTasksToolStripMenuItem,
            this.removeTasksToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addNewTaskMenuItem
            // 
            this.addNewTaskMenuItem.Name = "addNewTaskMenuItem";
            this.addNewTaskMenuItem.Size = new System.Drawing.Size(94, 20);
            this.addNewTaskMenuItem.Text = "Add New Task";
            this.addNewTaskMenuItem.Click += new System.EventHandler(this.addNewTaskMenuItem_Click);
            // 
            // showTasksToolStripMenuItem
            // 
            this.showTasksToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showImportantTasksMenuItem,
            this.showNonImportantTasksMenuItem,
            this.showAllTasksMenuItem});
            this.showTasksToolStripMenuItem.Name = "showTasksToolStripMenuItem";
            this.showTasksToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.showTasksToolStripMenuItem.Text = "Show Tasks";
            // 
            // removeTasksToolStripMenuItem
            // 
            this.removeTasksToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeSingleTaskMenuItem,
            this.removeAllTasksMenuItem});
            this.removeTasksToolStripMenuItem.Name = "removeTasksToolStripMenuItem";
            this.removeTasksToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.removeTasksToolStripMenuItem.Text = "Remove Tasks";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxImportant);
            this.groupBox1.Controls.Add(this.listBoxAllTasks);
            this.groupBox1.Controls.Add(this.dateTimePickerDueDate);
            this.groupBox1.Controls.Add(this.labelDueDate);
            this.groupBox1.Controls.Add(this.textBoxCurrentTask);
            this.groupBox1.Controls.Add(this.labelCurrentTask);
            this.groupBox1.Location = new System.Drawing.Point(74, 92);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(625, 327);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // dateTimePickerDueDate
            // 
            this.dateTimePickerDueDate.Location = new System.Drawing.Point(80, 120);
            this.dateTimePickerDueDate.Name = "dateTimePickerDueDate";
            this.dateTimePickerDueDate.Size = new System.Drawing.Size(332, 20);
            this.dateTimePickerDueDate.TabIndex = 3;
            // 
            // labelDueDate
            // 
            this.labelDueDate.AutoSize = true;
            this.labelDueDate.Location = new System.Drawing.Point(77, 89);
            this.labelDueDate.Name = "labelDueDate";
            this.labelDueDate.Size = new System.Drawing.Size(53, 13);
            this.labelDueDate.TabIndex = 2;
            this.labelDueDate.Text = "Due Date";
            // 
            // textBoxCurrentTask
            // 
            this.textBoxCurrentTask.Location = new System.Drawing.Point(80, 55);
            this.textBoxCurrentTask.Name = "textBoxCurrentTask";
            this.textBoxCurrentTask.Size = new System.Drawing.Size(332, 20);
            this.textBoxCurrentTask.TabIndex = 1;
            // 
            // labelCurrentTask
            // 
            this.labelCurrentTask.AutoSize = true;
            this.labelCurrentTask.Location = new System.Drawing.Point(77, 28);
            this.labelCurrentTask.Name = "labelCurrentTask";
            this.labelCurrentTask.Size = new System.Drawing.Size(31, 13);
            this.labelCurrentTask.TabIndex = 0;
            this.labelCurrentTask.Text = "Task";
            // 
            // removeSingleTaskMenuItem
            // 
            this.removeSingleTaskMenuItem.Name = "removeSingleTaskMenuItem";
            this.removeSingleTaskMenuItem.Size = new System.Drawing.Size(180, 22);
            this.removeSingleTaskMenuItem.Text = "Remove Single Task";
            this.removeSingleTaskMenuItem.Click += new System.EventHandler(this.removeSingleTaskMenuItem_Click);
            // 
            // removeAllTasksMenuItem
            // 
            this.removeAllTasksMenuItem.Name = "removeAllTasksMenuItem";
            this.removeAllTasksMenuItem.Size = new System.Drawing.Size(180, 22);
            this.removeAllTasksMenuItem.Text = "Remove All Tasks";
            this.removeAllTasksMenuItem.Click += new System.EventHandler(this.removeAllTasksMenuItem_Click);
            // 
            // showImportantTasksMenuItem
            // 
            this.showImportantTasksMenuItem.Name = "showImportantTasksMenuItem";
            this.showImportantTasksMenuItem.Size = new System.Drawing.Size(218, 22);
            this.showImportantTasksMenuItem.Text = "Show Important Tasks";
            // 
            // showNonImportantTasksMenuItem
            // 
            this.showNonImportantTasksMenuItem.Name = "showNonImportantTasksMenuItem";
            this.showNonImportantTasksMenuItem.Size = new System.Drawing.Size(218, 22);
            this.showNonImportantTasksMenuItem.Text = "Show Non-Important Tasks";
            // 
            // showAllTasksMenuItem
            // 
            this.showAllTasksMenuItem.Name = "showAllTasksMenuItem";
            this.showAllTasksMenuItem.Size = new System.Drawing.Size(218, 22);
            this.showAllTasksMenuItem.Text = "Show All Tasks";
            // 
            // listBoxAllTasks
            // 
            this.listBoxAllTasks.FormattingEnabled = true;
            this.listBoxAllTasks.Location = new System.Drawing.Point(80, 183);
            this.listBoxAllTasks.Name = "listBoxAllTasks";
            this.listBoxAllTasks.Size = new System.Drawing.Size(494, 108);
            this.listBoxAllTasks.TabIndex = 4;
            // 
            // checkBoxImportant
            // 
            this.checkBoxImportant.AutoSize = true;
            this.checkBoxImportant.Location = new System.Drawing.Point(431, 57);
            this.checkBoxImportant.Name = "checkBoxImportant";
            this.checkBoxImportant.Size = new System.Drawing.Size(90, 17);
            this.checkBoxImportant.TabIndex = 5;
            this.checkBoxImportant.Text = "IMPORTANT";
            this.checkBoxImportant.UseVisualStyleBackColor = true;
            // 
            // ToDoGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ToDoGUI";
            this.Text = "ToDoGUI";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addNewTaskMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showTasksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeTasksToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxCurrentTask;
        private System.Windows.Forms.Label labelCurrentTask;
        private System.Windows.Forms.DateTimePicker dateTimePickerDueDate;
        private System.Windows.Forms.Label labelDueDate;
        private System.Windows.Forms.ToolStripMenuItem showImportantTasksMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showNonImportantTasksMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showAllTasksMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeSingleTaskMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeAllTasksMenuItem;
        private System.Windows.Forms.ListBox listBoxAllTasks;
        private System.Windows.Forms.CheckBox checkBoxImportant;
    }
}

