﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ToDo
{
    class Program
    {

        static string[] toDo = {"" + "" + ""};
        static string[] dueDate = { };

        const int USERINPUTMAX = 3;
        const int USERINPUTMIN = 1;


        static void Main(string[] args)
        {

            for (; ; )
            {
                userPrompt();
            }

        }

        static void userPrompt()
        {

            string userInput = "";

            WriteLine("Welcome to the To-Do List application!" +
                      "\n Press 1 to add a new task." +
                      "\n Press 2 to remove a task." +
                      "\n Press 3 to remove ALL tasks.");

            userInput = ReadLine();

            if ((isNumeric(userInput)) && (Convert.ToInt32(userInput) >= USERINPUTMIN) && (Convert.ToInt32(userInput) <= USERINPUTMAX))
            {

                switch(userInput)
                {
                    case "1":
                        addNewTask();
                        return;

                    case "2":
                        removeTask();
                        return;

                    case "3":
                        removeAllTasks();
                        return;

                }

            }
            else
            {
                WriteLine("OOR or Non-Numeric Input");
                userPrompt();
            }

        }

        static void addNewTask()
        {
            WriteLine("\nAdd a '!' if the task is important!" +
                      "Task: ");
            string userInput = ReadLine();

            
            


        }



        /////
        static bool isNumeric(String input)
        {
            int test;
            return int.TryParse(input, out test);
        }
    }
}
