﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyDemoDB
{
    public partial class formInsertCustomer : Form
    {
        string connectionString = @"Data Source =(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\lukeg\Desktop\AWD 1100\MyDemoDB\MyDemoDB\demoDB.mdf;Integrated Security = true";

        public formInsertCustomer()
        {
            InitializeComponent();
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormLogin login = new FormLogin();
            login.ShowDialog();
        }


        private void buttonSave_Click(object sender, EventArgs e)
        {
            bool keepGoing = true;

            keepGoing = checkForNullCompanyName();

            if (keepGoing)
            {
                string company = textBoxCompanyName.Text.ToString();
                string contact = textBoxContactName.Text.ToString();
                string phone = textBoxPhoneNumber.Text.ToString();

                try
                {
                    //this will be the actual database connection
                    SqlConnection conn = new SqlConnection(connectionString);

                    //Open said connection
                    conn.Open();

                    //This will hold a string representing the actual SQL command
                    string sql = "INSERT INTO customers (companyName, contactName, phone) VALUES ('" + company + "','" + contact + "','" + phone + "') ;";

                    //This will hold the actual command and connection string
                    SqlCommand command = new SqlCommand(sql, conn);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(command);

                    dataAdapter.InsertCommand = new SqlCommand(sql, conn);
                    dataAdapter.InsertCommand.ExecuteNonQuery();

                    MessageBox.Show("Record Inserted",
                                    "SUCCESSFUL INSERT",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);

                    command.Dispose();
                    conn.Close();

                    clearInsertForm();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message,
                                    "EROR OCCURRED",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Record Insert Aborted",
                                "NO RECORD INSERTED",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);

                return;
            }
        }

        private bool checkForNullCompanyName()
        {
            if (textBoxCompanyName.Text.Trim() == "")
            {
                MessageBox.Show("Company Name CANNOT Be Empty!",
                                "EROR OCCURRED",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                textBoxCompanyName.Focus();
                return false;
            }

            return true;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            clearInsertForm();

            return;
        }

        private void clearInsertForm()
        {
            //Cancel transaction
            textBoxCompanyName.Text = "";
            textBoxContactName.Text = "";
            textBoxPhoneNumber.Text = "";
            textBoxCompanyName.Focus();
        }
    }
}
