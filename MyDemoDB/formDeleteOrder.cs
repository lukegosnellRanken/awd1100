﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyDemoDB
{
    public partial class formDeleteOrder : Form
    {
        string connectionString = @"Data Source =(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\lukeg\Desktop\AWD 1100\MyDemoDB\MyDemoDB\demoDB.mdf;Integrated Security = true";

        int cId = 0;

        public formDeleteOrder()
        {
            InitializeComponent();
        }

        private void FillComboBox()
        {
            //This will be the actual database connection
            SqlConnection conn = new SqlConnection(connectionString);

            //This string represents the query
            string sql = "SELECT * FROM customers ; ";

            //Open said connection
            conn.Open();

            //This will hold the actual command and connection string
            SqlCommand command = new SqlCommand(sql, conn);

            //Create a DataReader to hold the query results
            SqlDataReader dataReader;

            try
            {
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    cId = Convert.ToInt32(dataReader["CustomerID"].ToString());
                    comboBoxUpdateCustomer.Items.Add(cId);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            conn.Close();
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormLogin login = new FormLogin();
            login.ShowDialog();
        }

        private void comboBoxUpdateCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            //This will be the actual database conection
            SqlConnection conn = new SqlConnection(connectionString);

            //This string represents the query
            string sql = "SELECT * FROM customers WHERE customerID='" + comboBoxUpdateCustomer.Text + "' ;";

            //open said connection
            conn.Open();

            //This will hold the actual command and connection string
            SqlCommand command = new SqlCommand(sql, conn);

            //Create a DataReaderto hold the query results
            SqlDataReader dataReader;

            try
            {
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    cId = Convert.ToInt32(dataReader["CustomerID"].ToString());
                    string comName = dataReader["companyName"].ToString();
                    string conName = dataReader["contactName"].ToString();
                    string phone = dataReader["phone"].ToString();
                    textBoxCustomerID.Text = comName;
                    textBoxContactName.Text = conName;
                    textBoxPhoneNumber.Text = phone;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            conn.Close();
        }


        private void buttonSave_Click(object sender, EventArgs e)
        {
            bool keepGoing = true;

            keepGoing = checkForNullCompanyName();

            if (keepGoing)
            {
                DoYouReallyWantToUpdateThisCustomer();
            }

            if (keepGoing)
            {

                try
                {
                    //this will be the actual database connection
                    SqlConnection conn = new SqlConnection(connectionString);

                    SqlCommand command;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();

                    string sql = "UPDATE Customers SET CompanyName = '" + textBoxCompanyName.Text.ToString() +
                                                   "', ContactName = '" + textBoxContactName.Text.ToString() +
                                                   "', Phone = '" + textBoxPhoneNumber.Text.ToString() +
                                  "' WHERE customerID = '" + cId.ToString() + "'; ";

                    command = new SqlCommand(sql, conn);

                    conn.Open();

                    dataAdapter.UpdateCommand = new SqlCommand(sql, conn);
                    dataAdapter.UpdateCommand.ExecuteNonQuery();

                    command.Dispose();
                    conn.Close();

                    MessageBox.Show("Current Customer Record Updated",
                                    "CUSTOMER RECORD UPDATED",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);

                    clearUpdateForm();

                    comboBoxUpdateCustomer.Items.Remove(comboBoxUpdateCustomer.SelectedItem);
                    comboBoxUpdateCustomer.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            return;
        }

        private bool DoYouReallyWantToUpdateThisCustomer()
        {
            string msg = "Do You Really Want To Update This Customer?";

            if (MessageBox.Show(msg,
                                "UPDATE CURRENT CUSTOMER???",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Information) ==
                                DialogResult.Yes)
            {
                return true;
            }

            return false;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            clearUpdateForm();

            return;
        }

        private void clearUpdateForm()
        {
            //Cancel transaction
            textBoxCustomerID.Text = "";
            textBoxCompanyName.Text = "";
            textBoxContactName.Text = "";
            textBoxPhoneNumber.Text = "";
        }
    }
}
