﻿namespace MyDemoDB
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonShowCustomerData = new System.Windows.Forms.Button();
            this.buttonShowOrdersData = new System.Windows.Forms.Button();
            this.buttonInsertCustomer = new System.Windows.Forms.Button();
            this.buttonInsertOrder = new System.Windows.Forms.Button();
            this.buttonDeleteOrder = new System.Windows.Forms.Button();
            this.buttonDeleteCustomer = new System.Windows.Forms.Button();
            this.buttonUpdateOrder = new System.Windows.Forms.Button();
            this.buttonUpdateCustomer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonShowCustomerData
            // 
            this.buttonShowCustomerData.Location = new System.Drawing.Point(133, 63);
            this.buttonShowCustomerData.Name = "buttonShowCustomerData";
            this.buttonShowCustomerData.Size = new System.Drawing.Size(255, 108);
            this.buttonShowCustomerData.TabIndex = 0;
            this.buttonShowCustomerData.Text = "Customers";
            this.buttonShowCustomerData.UseVisualStyleBackColor = true;
            // 
            // buttonShowOrdersData
            // 
            this.buttonShowOrdersData.Location = new System.Drawing.Point(422, 63);
            this.buttonShowOrdersData.Name = "buttonShowOrdersData";
            this.buttonShowOrdersData.Size = new System.Drawing.Size(255, 108);
            this.buttonShowOrdersData.TabIndex = 1;
            this.buttonShowOrdersData.Text = "Orders";
            this.buttonShowOrdersData.UseVisualStyleBackColor = true;
            // 
            // buttonInsertCustomer
            // 
            this.buttonInsertCustomer.Location = new System.Drawing.Point(133, 202);
            this.buttonInsertCustomer.Name = "buttonInsertCustomer";
            this.buttonInsertCustomer.Size = new System.Drawing.Size(255, 108);
            this.buttonInsertCustomer.TabIndex = 2;
            this.buttonInsertCustomer.Text = "Insert Customer";
            this.buttonInsertCustomer.UseVisualStyleBackColor = true;
            this.buttonInsertCustomer.Click += new System.EventHandler(this.buttonInsertCustomer_Click);
            // 
            // buttonInsertOrder
            // 
            this.buttonInsertOrder.Location = new System.Drawing.Point(422, 202);
            this.buttonInsertOrder.Name = "buttonInsertOrder";
            this.buttonInsertOrder.Size = new System.Drawing.Size(255, 108);
            this.buttonInsertOrder.TabIndex = 3;
            this.buttonInsertOrder.Text = "Insert Order";
            this.buttonInsertOrder.UseVisualStyleBackColor = true;
            this.buttonInsertOrder.Click += new System.EventHandler(this.buttonInsertOrder_Click);
            // 
            // buttonDeleteOrder
            // 
            this.buttonDeleteOrder.Location = new System.Drawing.Point(422, 481);
            this.buttonDeleteOrder.Name = "buttonDeleteOrder";
            this.buttonDeleteOrder.Size = new System.Drawing.Size(255, 108);
            this.buttonDeleteOrder.TabIndex = 7;
            this.buttonDeleteOrder.Text = "Delete Order";
            this.buttonDeleteOrder.UseVisualStyleBackColor = true;
            this.buttonDeleteOrder.Click += new System.EventHandler(this.buttonDeleteOrder_Click);
            // 
            // buttonDeleteCustomer
            // 
            this.buttonDeleteCustomer.Location = new System.Drawing.Point(133, 481);
            this.buttonDeleteCustomer.Name = "buttonDeleteCustomer";
            this.buttonDeleteCustomer.Size = new System.Drawing.Size(255, 108);
            this.buttonDeleteCustomer.TabIndex = 6;
            this.buttonDeleteCustomer.Text = "Delete Customer";
            this.buttonDeleteCustomer.UseVisualStyleBackColor = true;
            this.buttonDeleteCustomer.Click += new System.EventHandler(this.buttonDeleteCustomer_Click);
            // 
            // buttonUpdateOrder
            // 
            this.buttonUpdateOrder.Location = new System.Drawing.Point(422, 342);
            this.buttonUpdateOrder.Name = "buttonUpdateOrder";
            this.buttonUpdateOrder.Size = new System.Drawing.Size(255, 108);
            this.buttonUpdateOrder.TabIndex = 5;
            this.buttonUpdateOrder.Text = "Update Order";
            this.buttonUpdateOrder.UseVisualStyleBackColor = true;
            this.buttonUpdateOrder.Click += new System.EventHandler(this.buttonUpdateOrder_Click);
            // 
            // buttonUpdateCustomer
            // 
            this.buttonUpdateCustomer.Location = new System.Drawing.Point(133, 342);
            this.buttonUpdateCustomer.Name = "buttonUpdateCustomer";
            this.buttonUpdateCustomer.Size = new System.Drawing.Size(255, 108);
            this.buttonUpdateCustomer.TabIndex = 4;
            this.buttonUpdateCustomer.Text = "Update Customer";
            this.buttonUpdateCustomer.UseVisualStyleBackColor = true;
            this.buttonUpdateCustomer.Click += new System.EventHandler(this.buttonUpdateCustomer_Click);
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(800, 628);
            this.Controls.Add(this.buttonDeleteOrder);
            this.Controls.Add(this.buttonDeleteCustomer);
            this.Controls.Add(this.buttonUpdateOrder);
            this.Controls.Add(this.buttonUpdateCustomer);
            this.Controls.Add(this.buttonInsertOrder);
            this.Controls.Add(this.buttonInsertCustomer);
            this.Controls.Add(this.buttonShowOrdersData);
            this.Controls.Add(this.buttonShowCustomerData);
            this.Name = "FormLogin";
            this.Text = "C# Database Connection Tutorial with Example";
            this.Load += new System.EventHandler(this.FormLogin_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonShowCustomerData;
        private System.Windows.Forms.Button buttonShowOrdersData;
        private System.Windows.Forms.Button buttonInsertCustomer;
        private System.Windows.Forms.Button buttonInsertOrder;
        private System.Windows.Forms.Button buttonDeleteOrder;
        private System.Windows.Forms.Button buttonDeleteCustomer;
        private System.Windows.Forms.Button buttonUpdateOrder;
        private System.Windows.Forms.Button buttonUpdateCustomer;
    }
}

