﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDemoDB
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }
        
        string connectionString =
            @"Data Source =(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\lukeg\Desktop\AWD 1100\MyDemoDB\MyDemoDB\demoDB.mdf;Integrated Security = true";

        //This will be our actual database connection
        SqlConnection conn = null;

        //This will hold the actual command and connection string
        SqlCommand command = null;

        SqlDataReader dataReader = null;

        //This will hold a string representing the actual SQL command
        string sql = "";

        //This will be the command output, for now just put in a MessageBox
        string output = "";

        //Code that runs when Customers button is clicked
        private void buttonShowCustomerData_Click(object sender, EventArgs e)
        {
            output = "";

            try
            {
                //Set the connection string
                conn = new SqlConnection(connectionString);

                //Open the database connection
                conn.Open();

                //Equivalent to SELECT * FROM customers, which works as well
                sql = "SELECT customerid, companyname, contactname, phone FROM customers";

                // A SqlCommand object allows you to query and send comands to a database. It has methods that are specialized for different commands.
                command = new SqlCommand(sql, conn);

                //The ExecuteReader method returns a SqlDataReader object for viewing the results of a selet query.
                dataReader = command.ExecuteReader();

                //Loop through the database and add the fields from each record to the output variable
                while (dataReader.Read())
                {
                    output = output + dataReader.GetValue(0) + " " + dataReader.GetValue(1) + " " + dataReader.GetValue(2) + " " + dataReader.GetValue(3) + "\n\n";
                }

                //Output record info
                MessageBox.Show(output, "Customer Records",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);

                //Close database conection
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonShowOrderData_Click(object sender, EventArgs e)
        {
            output = "";

            try
            {
                //Set the connection string
                conn = new SqlConnection(connectionString);

                //Open the database connection
                conn.Open();

                //Equivalent to SELECT * FROM orders, which works as well
                sql = "SELECT orderid, customerid, orderdate, orderquantity FROM orders";

                // A SqlCommand object allows you to query and send comands to a database. It has methods that are specialized for different commands.
                command = new SqlCommand(sql, conn);

                //The ExecuteReader method returns a SqlDataReader object for viewing the results of a selet query.
                dataReader = command.ExecuteReader();

                //Loop through the database and add the fields from each record to the output variable
                while (dataReader.Read())
                {
                    output = output + dataReader.GetValue(0) + " " + dataReader.GetValue(1) + " " + dataReader.GetValue(2) + " " + dataReader.GetValue(3) + "\n\n";
                }

                //Output record info
                MessageBox.Show(output, "Order Records",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);

                //Close database conection
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
