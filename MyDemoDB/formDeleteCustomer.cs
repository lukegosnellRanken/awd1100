﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyDemoDB
{
    public partial class formDeleteCustomer : Form
    {
        string connectionString = @"Data Source =(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\lukeg\Desktop\AWD 1100\MyDemoDB\MyDemoDB\demoDB.mdf;Integrated Security = true";

        int cId = 0;

        public formDeleteCustomer()
        {
            InitializeComponent();
            FillComboBox();
        }

        private void FillComboBox()
        {
            //This will be the actual database connection
            SqlConnection conn = new SqlConnection(connectionString);

            //This string represents the query
            string sql = "SELECT * FROM customers ; ";

            //Open said connection
            conn.Open();

            //This will hold the actual command and connection string
            SqlCommand command = new SqlCommand(sql, conn);

            //Create a DataReader to hold the query results
            SqlDataReader dataReader;

            try
            {
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    string cName =dataReader["CompanyName"].ToString();
                    comboBoxDeleteCustomer.Items.Add(cName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            conn.Close();
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormLogin login = new FormLogin();
            login.ShowDialog();
        }

        private void comboBoxDeleteCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            //This will be the actual database conection
            SqlConnection conn = new SqlConnection(connectionString);

            //This string represents the query
            string sql = "SELECT * FROM customers WHERE customerID='" + comboBoxDeleteCustomer.Text + "' ;";

            //open said connection
            conn.Open();

            //This will hold the actual command and connection string
            SqlCommand command = new SqlCommand(sql, conn);

            //Create a DataReaderto hold the query results
            SqlDataReader dataReader;

            try
            {
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    cId = Convert.ToInt32(dataReader["CustomerID"].ToString());
                    string comName = dataReader["companyName"].ToString();
                    string conName = dataReader["contactName"].ToString();
                    string phone = dataReader["phone"].ToString();
                    textBoxCustomerID.Text = comName;
                    textBoxContactName.Text = conName;
                    textBoxPhoneNumber.Text = phone;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            conn.Close();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormLogin login = new FormLogin();
            login.ShowDialog();
        }


        private void buttonSave_Click(object sender, EventArgs e)
        {
            bool keepGoing = true;

            if (keepGoing)
            {
                DoYouReallyWantToDeleteThisCustomer();
            }

            if (keepGoing)
            {

                try
                {
                    //this will be the actual database connection
                    SqlConnection conn = new SqlConnection(connectionString);

                    SqlCommand command;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();

                    string sql = "DELETE FROM customers WHERE customerid='" + cId + "' ;";

                    command = new SqlCommand(sql, conn);

                    conn.Open();

                    dataAdapter.UpdateCommand = new SqlCommand(sql, conn);
                    dataAdapter.UpdateCommand.ExecuteNonQuery();

                    command.Dispose();
                    conn.Close();

                    MessageBox.Show("Record Deleted",
                                    "CURRENT RECORD DELETED",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);

                    clearDeleteForm();

                    comboBoxDeleteCustomer.Items.Remove(comboBoxDeleteCustomer.SelectedItem);
                    comboBoxDeleteCustomer.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            return;
        }

        private bool DoYouReallyWantToDeleteThisCustomer()
        {
            string msg = "Do You Really Want To Delete This Customer?";
            msg += "\nNote: You will NOT be able to delete the";
            msg += "\ncustomer if they have any outstanding orders!";

            if (MessageBox.Show(msg,
                                "DELETE CURRENT CUSTOMER???",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Information) ==
                                DialogResult.Yes)
            {
                return true;
            }

            return false;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            clearDeleteForm();

            return;
        }

        private void clearDeleteForm()
        {
            //Cancel transaction
            textBoxCustomerID.Text = "";
            textBoxCompanyName.Text = "";
            textBoxContactName.Text = "";
            textBoxPhoneNumber.Text = "";
            comboBoxDeleteCustomer.Focus();
        }
    }
}
