﻿namespace MyDemoDB
{
    partial class formUpdateOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.labelOrderQuantity = new System.Windows.Forms.Label();
            this.labelOrderDate = new System.Windows.Forms.Label();
            this.textOrderQuantity = new System.Windows.Forms.TextBox();
            this.textBoxOrderDate = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelCustomerID = new System.Windows.Forms.Label();
            this.labelOrderID = new System.Windows.Forms.Label();
            this.labelOrderToUpdate = new System.Windows.Forms.Label();
            this.textBoxCustomerID = new System.Windows.Forms.TextBox();
            this.textBoxOrderID = new System.Windows.Forms.TextBox();
            this.textBoxOrderToUpdate = new System.Windows.Forms.TextBox();
            this.labelUpdateOrder = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.Location = new System.Drawing.Point(12, 382);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(304, 56);
            this.buttonGoBack.TabIndex = 1;
            this.buttonGoBack.Text = "Go Back";
            this.buttonGoBack.UseVisualStyleBackColor = true;
            // 
            // labelOrderQuantity
            // 
            this.labelOrderQuantity.AutoSize = true;
            this.labelOrderQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrderQuantity.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelOrderQuantity.Location = new System.Drawing.Point(96, 329);
            this.labelOrderQuantity.Name = "labelOrderQuantity";
            this.labelOrderQuantity.Size = new System.Drawing.Size(173, 25);
            this.labelOrderQuantity.TabIndex = 37;
            this.labelOrderQuantity.Text = "Order Quantity:";
            // 
            // labelOrderDate
            // 
            this.labelOrderDate.AutoSize = true;
            this.labelOrderDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrderDate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelOrderDate.Location = new System.Drawing.Point(135, 267);
            this.labelOrderDate.Name = "labelOrderDate";
            this.labelOrderDate.Size = new System.Drawing.Size(134, 25);
            this.labelOrderDate.TabIndex = 36;
            this.labelOrderDate.Text = "Order Date:";
            // 
            // textOrderQuantity
            // 
            this.textOrderQuantity.Location = new System.Drawing.Point(284, 335);
            this.textOrderQuantity.Name = "textOrderQuantity";
            this.textOrderQuantity.Size = new System.Drawing.Size(456, 20);
            this.textOrderQuantity.TabIndex = 35;
            // 
            // textBoxOrderDate
            // 
            this.textBoxOrderDate.Location = new System.Drawing.Point(284, 273);
            this.textBoxOrderDate.Name = "textBoxOrderDate";
            this.textBoxOrderDate.Size = new System.Drawing.Size(456, 20);
            this.textBoxOrderDate.TabIndex = 34;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(570, 382);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(217, 56);
            this.buttonSave.TabIndex = 33;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(334, 382);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(217, 56);
            this.buttonCancel.TabIndex = 32;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // labelCustomerID
            // 
            this.labelCustomerID.AutoSize = true;
            this.labelCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerID.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCustomerID.Location = new System.Drawing.Point(121, 211);
            this.labelCustomerID.Name = "labelCustomerID";
            this.labelCustomerID.Size = new System.Drawing.Size(148, 25);
            this.labelCustomerID.TabIndex = 31;
            this.labelCustomerID.Text = "Customer ID:";
            // 
            // labelOrderID
            // 
            this.labelOrderID.AutoSize = true;
            this.labelOrderID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrderID.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelOrderID.Location = new System.Drawing.Point(162, 149);
            this.labelOrderID.Name = "labelOrderID";
            this.labelOrderID.Size = new System.Drawing.Size(107, 25);
            this.labelOrderID.TabIndex = 30;
            this.labelOrderID.Text = "Order ID:";
            // 
            // labelOrderToUpdate
            // 
            this.labelOrderToUpdate.AutoSize = true;
            this.labelOrderToUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrderToUpdate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelOrderToUpdate.Location = new System.Drawing.Point(75, 92);
            this.labelOrderToUpdate.Name = "labelOrderToUpdate";
            this.labelOrderToUpdate.Size = new System.Drawing.Size(194, 25);
            this.labelOrderToUpdate.TabIndex = 29;
            this.labelOrderToUpdate.Text = "Order To Update:";
            // 
            // textBoxCustomerID
            // 
            this.textBoxCustomerID.Location = new System.Drawing.Point(284, 217);
            this.textBoxCustomerID.Name = "textBoxCustomerID";
            this.textBoxCustomerID.Size = new System.Drawing.Size(456, 20);
            this.textBoxCustomerID.TabIndex = 28;
            // 
            // textBoxOrderID
            // 
            this.textBoxOrderID.Location = new System.Drawing.Point(284, 155);
            this.textBoxOrderID.Name = "textBoxOrderID";
            this.textBoxOrderID.Size = new System.Drawing.Size(456, 20);
            this.textBoxOrderID.TabIndex = 27;
            // 
            // textBoxOrderToUpdate
            // 
            this.textBoxOrderToUpdate.Location = new System.Drawing.Point(284, 97);
            this.textBoxOrderToUpdate.Name = "textBoxOrderToUpdate";
            this.textBoxOrderToUpdate.Size = new System.Drawing.Size(456, 20);
            this.textBoxOrderToUpdate.TabIndex = 26;
            // 
            // labelUpdateOrder
            // 
            this.labelUpdateOrder.AutoSize = true;
            this.labelUpdateOrder.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelUpdateOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelUpdateOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUpdateOrder.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelUpdateOrder.Location = new System.Drawing.Point(158, 20);
            this.labelUpdateOrder.Name = "labelUpdateOrder";
            this.labelUpdateOrder.Size = new System.Drawing.Size(501, 55);
            this.labelUpdateOrder.TabIndex = 25;
            this.labelUpdateOrder.Text = "UPDATE AN ORDER";
            // 
            // formUpdateOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Olive;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelOrderQuantity);
            this.Controls.Add(this.labelOrderDate);
            this.Controls.Add(this.textOrderQuantity);
            this.Controls.Add(this.textBoxOrderDate);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelCustomerID);
            this.Controls.Add(this.labelOrderID);
            this.Controls.Add(this.labelOrderToUpdate);
            this.Controls.Add(this.textBoxCustomerID);
            this.Controls.Add(this.textBoxOrderID);
            this.Controls.Add(this.textBoxOrderToUpdate);
            this.Controls.Add(this.labelUpdateOrder);
            this.Controls.Add(this.buttonGoBack);
            this.Name = "formUpdateOrder";
            this.Text = "formUpdateOrder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.Label labelOrderQuantity;
        private System.Windows.Forms.Label labelOrderDate;
        private System.Windows.Forms.TextBox textOrderQuantity;
        private System.Windows.Forms.TextBox textBoxOrderDate;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelCustomerID;
        private System.Windows.Forms.Label labelOrderID;
        private System.Windows.Forms.Label labelOrderToUpdate;
        private System.Windows.Forms.TextBox textBoxCustomerID;
        private System.Windows.Forms.TextBox textBoxOrderID;
        private System.Windows.Forms.TextBox textBoxOrderToUpdate;
        private System.Windows.Forms.Label labelUpdateOrder;
    }
}