﻿namespace MyDemoDB
{
    partial class formUpdateCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelCompanyName = new System.Windows.Forms.Label();
            this.labelCustomerID = new System.Windows.Forms.Label();
            this.labelCustomerToUpdate = new System.Windows.Forms.Label();
            this.textBoxCompanyName = new System.Windows.Forms.TextBox();
            this.textBoxCustomerID = new System.Windows.Forms.TextBox();
            this.labelUpdateCustomer = new System.Windows.Forms.Label();
            this.labelPhoneNumber = new System.Windows.Forms.Label();
            this.labelContactName = new System.Windows.Forms.Label();
            this.textBoxPhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxContactName = new System.Windows.Forms.TextBox();
            this.comboBoxUpdateCustomer = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.Location = new System.Drawing.Point(12, 382);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(304, 56);
            this.buttonGoBack.TabIndex = 1;
            this.buttonGoBack.Text = "Go Back";
            this.buttonGoBack.UseVisualStyleBackColor = true;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(568, 382);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(217, 56);
            this.buttonSave.TabIndex = 20;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(332, 382);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(217, 56);
            this.buttonCancel.TabIndex = 19;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.AutoSize = true;
            this.labelCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompanyName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCompanyName.Location = new System.Drawing.Point(83, 211);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Size = new System.Drawing.Size(184, 25);
            this.labelCompanyName.TabIndex = 18;
            this.labelCompanyName.Text = "Company Name:";
            // 
            // labelCustomerID
            // 
            this.labelCustomerID.AutoSize = true;
            this.labelCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerID.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCustomerID.Location = new System.Drawing.Point(119, 149);
            this.labelCustomerID.Name = "labelCustomerID";
            this.labelCustomerID.Size = new System.Drawing.Size(148, 25);
            this.labelCustomerID.TabIndex = 17;
            this.labelCustomerID.Text = "Customer ID:";
            // 
            // labelCustomerToUpdate
            // 
            this.labelCustomerToUpdate.AutoSize = true;
            this.labelCustomerToUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerToUpdate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCustomerToUpdate.Location = new System.Drawing.Point(32, 91);
            this.labelCustomerToUpdate.Name = "labelCustomerToUpdate";
            this.labelCustomerToUpdate.Size = new System.Drawing.Size(235, 25);
            this.labelCustomerToUpdate.TabIndex = 16;
            this.labelCustomerToUpdate.Text = "Customer To Update:";
            // 
            // textBoxCompanyName
            // 
            this.textBoxCompanyName.Location = new System.Drawing.Point(282, 217);
            this.textBoxCompanyName.Name = "textBoxCompanyName";
            this.textBoxCompanyName.Size = new System.Drawing.Size(456, 20);
            this.textBoxCompanyName.TabIndex = 15;
            // 
            // textBoxCustomerID
            // 
            this.textBoxCustomerID.Location = new System.Drawing.Point(282, 155);
            this.textBoxCustomerID.Name = "textBoxCustomerID";
            this.textBoxCustomerID.Size = new System.Drawing.Size(456, 20);
            this.textBoxCustomerID.TabIndex = 14;
            // 
            // labelUpdateCustomer
            // 
            this.labelUpdateCustomer.AutoSize = true;
            this.labelUpdateCustomer.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelUpdateCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelUpdateCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUpdateCustomer.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelUpdateCustomer.Location = new System.Drawing.Point(124, 18);
            this.labelUpdateCustomer.Name = "labelUpdateCustomer";
            this.labelUpdateCustomer.Size = new System.Drawing.Size(569, 55);
            this.labelUpdateCustomer.TabIndex = 12;
            this.labelUpdateCustomer.Text = "UPDATE A CUSTOMER";
            // 
            // labelPhoneNumber
            // 
            this.labelPhoneNumber.AutoSize = true;
            this.labelPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhoneNumber.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelPhoneNumber.Location = new System.Drawing.Point(93, 329);
            this.labelPhoneNumber.Name = "labelPhoneNumber";
            this.labelPhoneNumber.Size = new System.Drawing.Size(174, 25);
            this.labelPhoneNumber.TabIndex = 24;
            this.labelPhoneNumber.Text = "Phone Number:";
            // 
            // labelContactName
            // 
            this.labelContactName.AutoSize = true;
            this.labelContactName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelContactName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelContactName.Location = new System.Drawing.Point(100, 267);
            this.labelContactName.Name = "labelContactName";
            this.labelContactName.Size = new System.Drawing.Size(167, 25);
            this.labelContactName.TabIndex = 23;
            this.labelContactName.Text = "Contact Name:";
            // 
            // textBoxPhoneNumber
            // 
            this.textBoxPhoneNumber.Location = new System.Drawing.Point(282, 335);
            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
            this.textBoxPhoneNumber.Size = new System.Drawing.Size(456, 20);
            this.textBoxPhoneNumber.TabIndex = 22;
            // 
            // textBoxContactName
            // 
            this.textBoxContactName.Location = new System.Drawing.Point(282, 273);
            this.textBoxContactName.Name = "textBoxContactName";
            this.textBoxContactName.Size = new System.Drawing.Size(456, 20);
            this.textBoxContactName.TabIndex = 21;
            // 
            // comboBoxUpdateCustomer
            // 
            this.comboBoxUpdateCustomer.FormattingEnabled = true;
            this.comboBoxUpdateCustomer.Location = new System.Drawing.Point(282, 97);
            this.comboBoxUpdateCustomer.Name = "comboBoxUpdateCustomer";
            this.comboBoxUpdateCustomer.Size = new System.Drawing.Size(456, 21);
            this.comboBoxUpdateCustomer.TabIndex = 25;
            // 
            // formUpdateCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Olive;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.comboBoxUpdateCustomer);
            this.Controls.Add(this.labelPhoneNumber);
            this.Controls.Add(this.labelContactName);
            this.Controls.Add(this.textBoxPhoneNumber);
            this.Controls.Add(this.textBoxContactName);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelCompanyName);
            this.Controls.Add(this.labelCustomerID);
            this.Controls.Add(this.labelCustomerToUpdate);
            this.Controls.Add(this.textBoxCompanyName);
            this.Controls.Add(this.textBoxCustomerID);
            this.Controls.Add(this.labelUpdateCustomer);
            this.Controls.Add(this.buttonGoBack);
            this.Name = "formUpdateCustomer";
            this.Text = "formUpdateCustomer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelCompanyName;
        private System.Windows.Forms.Label labelCustomerID;
        private System.Windows.Forms.Label labelCustomerToUpdate;
        private System.Windows.Forms.TextBox textBoxCompanyName;
        private System.Windows.Forms.TextBox textBoxCustomerID;
        private System.Windows.Forms.Label labelUpdateCustomer;
        private System.Windows.Forms.Label labelPhoneNumber;
        private System.Windows.Forms.Label labelContactName;
        private System.Windows.Forms.TextBox textBoxPhoneNumber;
        private System.Windows.Forms.TextBox textBoxContactName;
        private System.Windows.Forms.ComboBox comboBoxUpdateCustomer;
    }
}