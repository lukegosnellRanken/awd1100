﻿namespace MyDemoDB
{
    partial class formDeleteOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.labelOrderQuantity = new System.Windows.Forms.Label();
            this.labelOrderDate = new System.Windows.Forms.Label();
            this.textOrderQuantity = new System.Windows.Forms.TextBox();
            this.textBoxOrderDate = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelCustomerID = new System.Windows.Forms.Label();
            this.labelOrderID = new System.Windows.Forms.Label();
            this.labelOrderToDelete = new System.Windows.Forms.Label();
            this.textBoxCustomerID = new System.Windows.Forms.TextBox();
            this.textBoxOrderID = new System.Windows.Forms.TextBox();
            this.labelUpdateOrder = new System.Windows.Forms.Label();
            this.comboBoxDeleteCustomer = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.Location = new System.Drawing.Point(12, 382);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(304, 56);
            this.buttonGoBack.TabIndex = 0;
            this.buttonGoBack.Text = "Go Back";
            this.buttonGoBack.UseVisualStyleBackColor = true;
            // 
            // labelOrderQuantity
            // 
            this.labelOrderQuantity.AutoSize = true;
            this.labelOrderQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrderQuantity.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelOrderQuantity.Location = new System.Drawing.Point(96, 328);
            this.labelOrderQuantity.Name = "labelOrderQuantity";
            this.labelOrderQuantity.Size = new System.Drawing.Size(173, 25);
            this.labelOrderQuantity.TabIndex = 50;
            this.labelOrderQuantity.Text = "Order Quantity:";
            // 
            // labelOrderDate
            // 
            this.labelOrderDate.AutoSize = true;
            this.labelOrderDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrderDate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelOrderDate.Location = new System.Drawing.Point(135, 266);
            this.labelOrderDate.Name = "labelOrderDate";
            this.labelOrderDate.Size = new System.Drawing.Size(134, 25);
            this.labelOrderDate.TabIndex = 49;
            this.labelOrderDate.Text = "Order Date:";
            // 
            // textOrderQuantity
            // 
            this.textOrderQuantity.Location = new System.Drawing.Point(284, 334);
            this.textOrderQuantity.Name = "textOrderQuantity";
            this.textOrderQuantity.Size = new System.Drawing.Size(456, 20);
            this.textOrderQuantity.TabIndex = 48;
            // 
            // textBoxOrderDate
            // 
            this.textBoxOrderDate.Location = new System.Drawing.Point(284, 272);
            this.textBoxOrderDate.Name = "textBoxOrderDate";
            this.textBoxOrderDate.Size = new System.Drawing.Size(456, 20);
            this.textBoxOrderDate.TabIndex = 47;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(570, 381);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(217, 56);
            this.buttonSave.TabIndex = 46;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(334, 381);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(217, 56);
            this.buttonCancel.TabIndex = 45;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // labelCustomerID
            // 
            this.labelCustomerID.AutoSize = true;
            this.labelCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerID.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCustomerID.Location = new System.Drawing.Point(121, 210);
            this.labelCustomerID.Name = "labelCustomerID";
            this.labelCustomerID.Size = new System.Drawing.Size(148, 25);
            this.labelCustomerID.TabIndex = 44;
            this.labelCustomerID.Text = "Customer ID:";
            // 
            // labelOrderID
            // 
            this.labelOrderID.AutoSize = true;
            this.labelOrderID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrderID.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelOrderID.Location = new System.Drawing.Point(162, 148);
            this.labelOrderID.Name = "labelOrderID";
            this.labelOrderID.Size = new System.Drawing.Size(107, 25);
            this.labelOrderID.TabIndex = 43;
            this.labelOrderID.Text = "Order ID:";
            // 
            // labelOrderToDelete
            // 
            this.labelOrderToDelete.AutoSize = true;
            this.labelOrderToDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrderToDelete.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelOrderToDelete.Location = new System.Drawing.Point(75, 91);
            this.labelOrderToDelete.Name = "labelOrderToDelete";
            this.labelOrderToDelete.Size = new System.Drawing.Size(187, 25);
            this.labelOrderToDelete.TabIndex = 42;
            this.labelOrderToDelete.Text = "Order To Delete:";
            // 
            // textBoxCustomerID
            // 
            this.textBoxCustomerID.Location = new System.Drawing.Point(284, 216);
            this.textBoxCustomerID.Name = "textBoxCustomerID";
            this.textBoxCustomerID.Size = new System.Drawing.Size(456, 20);
            this.textBoxCustomerID.TabIndex = 41;
            // 
            // textBoxOrderID
            // 
            this.textBoxOrderID.Location = new System.Drawing.Point(284, 154);
            this.textBoxOrderID.Name = "textBoxOrderID";
            this.textBoxOrderID.Size = new System.Drawing.Size(456, 20);
            this.textBoxOrderID.TabIndex = 40;
            // 
            // labelUpdateOrder
            // 
            this.labelUpdateOrder.AutoSize = true;
            this.labelUpdateOrder.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelUpdateOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelUpdateOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUpdateOrder.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelUpdateOrder.Location = new System.Drawing.Point(158, 19);
            this.labelUpdateOrder.Name = "labelUpdateOrder";
            this.labelUpdateOrder.Size = new System.Drawing.Size(493, 55);
            this.labelUpdateOrder.TabIndex = 38;
            this.labelUpdateOrder.Text = "DELETE AN ORDER";
            // 
            // comboBoxDeleteCustomer
            // 
            this.comboBoxDeleteCustomer.FormattingEnabled = true;
            this.comboBoxDeleteCustomer.Location = new System.Drawing.Point(284, 97);
            this.comboBoxDeleteCustomer.Name = "comboBoxDeleteCustomer";
            this.comboBoxDeleteCustomer.Size = new System.Drawing.Size(456, 21);
            this.comboBoxDeleteCustomer.TabIndex = 51;
            // 
            // formDeleteOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.comboBoxDeleteCustomer);
            this.Controls.Add(this.labelOrderQuantity);
            this.Controls.Add(this.labelOrderDate);
            this.Controls.Add(this.textOrderQuantity);
            this.Controls.Add(this.textBoxOrderDate);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelCustomerID);
            this.Controls.Add(this.labelOrderID);
            this.Controls.Add(this.labelOrderToDelete);
            this.Controls.Add(this.textBoxCustomerID);
            this.Controls.Add(this.textBoxOrderID);
            this.Controls.Add(this.labelUpdateOrder);
            this.Controls.Add(this.buttonGoBack);
            this.Name = "formDeleteOrder";
            this.Text = "formDeleteOrder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.Label labelOrderQuantity;
        private System.Windows.Forms.Label labelOrderDate;
        private System.Windows.Forms.TextBox textOrderQuantity;
        private System.Windows.Forms.TextBox textBoxOrderDate;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelCustomerID;
        private System.Windows.Forms.Label labelOrderID;
        private System.Windows.Forms.Label labelOrderToDelete;
        private System.Windows.Forms.TextBox textBoxCustomerID;
        private System.Windows.Forms.TextBox textBoxOrderID;
        private System.Windows.Forms.Label labelUpdateOrder;
        private System.Windows.Forms.ComboBox comboBoxDeleteCustomer;
    }
}