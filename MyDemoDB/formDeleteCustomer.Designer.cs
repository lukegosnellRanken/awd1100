﻿namespace MyDemoDB
{
    partial class formDeleteCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.labelPhoneNumber = new System.Windows.Forms.Label();
            this.labelContactName = new System.Windows.Forms.Label();
            this.textBoxPhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxContactName = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelCompanyName = new System.Windows.Forms.Label();
            this.labelCustomerID = new System.Windows.Forms.Label();
            this.labelCustomerToDelete = new System.Windows.Forms.Label();
            this.textBoxCompanyName = new System.Windows.Forms.TextBox();
            this.textBoxCustomerID = new System.Windows.Forms.TextBox();
            this.labelDeleteCustomer = new System.Windows.Forms.Label();
            this.comboBoxDeleteCustomer = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.Location = new System.Drawing.Point(12, 382);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(304, 56);
            this.buttonGoBack.TabIndex = 1;
            this.buttonGoBack.Text = "Go Back";
            this.buttonGoBack.UseVisualStyleBackColor = true;
            // 
            // labelPhoneNumber
            // 
            this.labelPhoneNumber.AutoSize = true;
            this.labelPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhoneNumber.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelPhoneNumber.Location = new System.Drawing.Point(94, 328);
            this.labelPhoneNumber.Name = "labelPhoneNumber";
            this.labelPhoneNumber.Size = new System.Drawing.Size(174, 25);
            this.labelPhoneNumber.TabIndex = 37;
            this.labelPhoneNumber.Text = "Phone Number:";
            // 
            // labelContactName
            // 
            this.labelContactName.AutoSize = true;
            this.labelContactName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelContactName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelContactName.Location = new System.Drawing.Point(101, 266);
            this.labelContactName.Name = "labelContactName";
            this.labelContactName.Size = new System.Drawing.Size(167, 25);
            this.labelContactName.TabIndex = 36;
            this.labelContactName.Text = "Contact Name:";
            // 
            // textBoxPhoneNumber
            // 
            this.textBoxPhoneNumber.Location = new System.Drawing.Point(283, 334);
            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
            this.textBoxPhoneNumber.Size = new System.Drawing.Size(456, 20);
            this.textBoxPhoneNumber.TabIndex = 35;
            // 
            // textBoxContactName
            // 
            this.textBoxContactName.Location = new System.Drawing.Point(283, 272);
            this.textBoxContactName.Name = "textBoxContactName";
            this.textBoxContactName.Size = new System.Drawing.Size(456, 20);
            this.textBoxContactName.TabIndex = 34;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(569, 381);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(217, 56);
            this.buttonSave.TabIndex = 33;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(333, 381);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(217, 56);
            this.buttonCancel.TabIndex = 32;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.AutoSize = true;
            this.labelCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompanyName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCompanyName.Location = new System.Drawing.Point(84, 210);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Size = new System.Drawing.Size(184, 25);
            this.labelCompanyName.TabIndex = 31;
            this.labelCompanyName.Text = "Company Name:";
            // 
            // labelCustomerID
            // 
            this.labelCustomerID.AutoSize = true;
            this.labelCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerID.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCustomerID.Location = new System.Drawing.Point(120, 148);
            this.labelCustomerID.Name = "labelCustomerID";
            this.labelCustomerID.Size = new System.Drawing.Size(148, 25);
            this.labelCustomerID.TabIndex = 30;
            this.labelCustomerID.Text = "Customer ID:";
            // 
            // labelCustomerToDelete
            // 
            this.labelCustomerToDelete.AutoSize = true;
            this.labelCustomerToDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerToDelete.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCustomerToDelete.Location = new System.Drawing.Point(40, 90);
            this.labelCustomerToDelete.Name = "labelCustomerToDelete";
            this.labelCustomerToDelete.Size = new System.Drawing.Size(228, 25);
            this.labelCustomerToDelete.TabIndex = 29;
            this.labelCustomerToDelete.Text = "Customer To Delete:";
            // 
            // textBoxCompanyName
            // 
            this.textBoxCompanyName.Location = new System.Drawing.Point(283, 216);
            this.textBoxCompanyName.Name = "textBoxCompanyName";
            this.textBoxCompanyName.Size = new System.Drawing.Size(456, 20);
            this.textBoxCompanyName.TabIndex = 28;
            // 
            // textBoxCustomerID
            // 
            this.textBoxCustomerID.Location = new System.Drawing.Point(283, 154);
            this.textBoxCustomerID.Name = "textBoxCustomerID";
            this.textBoxCustomerID.Size = new System.Drawing.Size(456, 20);
            this.textBoxCustomerID.TabIndex = 27;
            // 
            // labelDeleteCustomer
            // 
            this.labelDeleteCustomer.AutoSize = true;
            this.labelDeleteCustomer.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelDeleteCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelDeleteCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeleteCustomer.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelDeleteCustomer.Location = new System.Drawing.Point(125, 17);
            this.labelDeleteCustomer.Name = "labelDeleteCustomer";
            this.labelDeleteCustomer.Size = new System.Drawing.Size(561, 55);
            this.labelDeleteCustomer.TabIndex = 25;
            this.labelDeleteCustomer.Text = "DELETE A CUSTOMER";
            // 
            // comboBoxDeleteCustomer
            // 
            this.comboBoxDeleteCustomer.FormattingEnabled = true;
            this.comboBoxDeleteCustomer.Location = new System.Drawing.Point(283, 96);
            this.comboBoxDeleteCustomer.Name = "comboBoxDeleteCustomer";
            this.comboBoxDeleteCustomer.Size = new System.Drawing.Size(456, 21);
            this.comboBoxDeleteCustomer.TabIndex = 38;
            // 
            // formDeleteCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.comboBoxDeleteCustomer);
            this.Controls.Add(this.labelPhoneNumber);
            this.Controls.Add(this.labelContactName);
            this.Controls.Add(this.textBoxPhoneNumber);
            this.Controls.Add(this.textBoxContactName);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelCompanyName);
            this.Controls.Add(this.labelCustomerID);
            this.Controls.Add(this.labelCustomerToDelete);
            this.Controls.Add(this.textBoxCompanyName);
            this.Controls.Add(this.textBoxCustomerID);
            this.Controls.Add(this.labelDeleteCustomer);
            this.Controls.Add(this.buttonGoBack);
            this.Name = "formDeleteCustomer";
            this.Text = "formDeleteCustomer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.Label labelPhoneNumber;
        private System.Windows.Forms.Label labelContactName;
        private System.Windows.Forms.TextBox textBoxPhoneNumber;
        private System.Windows.Forms.TextBox textBoxContactName;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelCompanyName;
        private System.Windows.Forms.Label labelCustomerID;
        private System.Windows.Forms.Label labelCustomerToDelete;
        private System.Windows.Forms.TextBox textBoxCompanyName;
        private System.Windows.Forms.TextBox textBoxCustomerID;
        private System.Windows.Forms.Label labelDeleteCustomer;
        private System.Windows.Forms.ComboBox comboBoxDeleteCustomer;
    }
}