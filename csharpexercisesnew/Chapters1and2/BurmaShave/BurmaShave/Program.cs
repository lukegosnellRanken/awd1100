﻿using System;
using static System.Console;

namespace BurmaShave
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("\n\n\n");
            WriteLine("\t\tShave the modern way");
            WriteLine("\t\tNo brush");
            WriteLine("\t\tNo lather");
            WriteLine("\t\tBig tube 35 cents");
            WriteLine("\t\tBurma-Shave");
            ReadLine();
        }
    }
}
