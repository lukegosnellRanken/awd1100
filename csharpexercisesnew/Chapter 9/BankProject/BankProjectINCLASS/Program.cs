﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace BankProjectINCLASS
{
    class Program
    {
        const int ARRAYSIZE = 5;

        static void Main(string[] args)
        {
            //create bank account array
            BankAccount[] ba = new BankAccount[ARRAYSIZE];

            //input data into bank account
            inputBankAccountObjects(ba);

            //Print out data for each bank account array
            printOutBankAccountObjectViaOverloadedToString(ba);

            //sort BankAccount array by last name
            Array.Sort(ba);

            //print out data for all BankAccount array (ba) element in a kind of list like format
            printOutBankAccountCustomerList(ba);

            //let user search through BankAccoutn array looking for customer with last name
            searchForBankAccountCustomerByLastName(ba);
        }

        private static void inputBankAccountObjects(BankAccount[] ba)
        {
            int anVal = 0;
            double ibVal = 0.0;
            string firstName = "";
            string lastName = "";
            int acctNumber = 0;
            double initBalance = 0.0;

            //Fill up bank account arrat (ba)
            for (int lcv = 0; lcv < ba.Length; ++lcv)
            {
                Console.Clear();
                Write("Enter customer {0} first name\t\t", (lcv + 1));
                firstName = ReadLine();

                Write("Enter customer {0} last name\t\t", (lcv + 1));
                lastName = ReadLine();

                Write("Enter customer {0} account number\t\t", (lcv + 1));
                int.TryParse(ReadLine(), out anVal);
                acctNumber = anVal;

                Write("Enter customer {0} initial balance\t\t", (lcv + 1));
                double.TryParse(ReadLine(), out ibVal);
                initBalance = ibVal;

                //create new ba using above info
                ba[lcv] = new BankAccount(firstName.ToUpper(), lastName.ToUpper(), acctNumber, initBalance);
            }
        }

        private static void printOutBankAccountObjectViaOverloadedToString(BankAccount[] ba)
        {
            for (int lcv = 0; lcv < ba.Length; ++lcv)
            {
                Console.Clear();
                WriteLine("BankAccount object number {0}", (lcv + 1));
                WriteLine(ba[lcv]);

                ReadLine();
                Console.Clear();
            }
        }

        private static void printOutBankAccountCustomerList(BankAccount[] ba)
        {
            WriteLine("Your Sorted Bank Account Customers");
            WriteLine("\n\tNAME\t\t\tACCT NUMBER\t\tBALANCE");
            WriteLine("==============================================================");

            for (int lcv = 0; lcv < ba.Length; ++lcv)
            {
                WriteLine("{0}\t\t\t{1}\t\t\t{2}",
                          ba[lcv].LastName + ", " +
                          ba[lcv].FirstName ,
                          ba[lcv].AcctNumber.ToString() ,
                          ba[lcv].InitBalance.ToString("C"));
            }

            ReadLine();
        }

        private static void searchForBankAccountCustomerByLastName(BankAccount[] ba)
        {
            string lastName = "";
            bool custFound = false;

            Console.Clear();
            Write("Enter Customer Last Name To Search For: ");
            lastName = ReadLine();

            for (int lcv = 0; lcv < ba.Length; ++lcv)
            {
                if (ba[lcv].LastName.Equals(lastName.ToUpper()))
                {
                    WriteLine("\nCustomer(s) with last name of {0}: " , lastName);
                    Write("First Name:\t\t", ba[lcv].FirstName);
                    Write("Last Name:\t\t", ba[lcv].LastName);
                    Write("Account Number:\t\t", ba[lcv].AcctNumber);
                    WriteLine("Initial Balance:\t\t", ba[lcv].InitBalance);
                    custFound = true;
                }

                if (!custFound)
                {
                    WriteLine("No Customer Last Name:\t{0}", lastName);
                }
            }

            ReadLine();
            Environment.Exit{0};
        }
    }
}
