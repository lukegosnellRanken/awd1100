﻿namespace ProjectedRaisesGUI
{
    partial class ProjectedRaisesGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.textBoxSalary = new System.Windows.Forms.TextBox();
            this.textBoxSalaryNew = new System.Windows.Forms.TextBox();
            this.labelSalary = new System.Windows.Forms.Label();
            this.labelSalaryNew = new System.Windows.Forms.Label();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Location = new System.Drawing.Point(189, 362);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(419, 43);
            this.buttonCalculate.TabIndex = 2;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // textBoxSalary
            // 
            this.textBoxSalary.Location = new System.Drawing.Point(365, 92);
            this.textBoxSalary.Name = "textBoxSalary";
            this.textBoxSalary.Size = new System.Drawing.Size(243, 20);
            this.textBoxSalary.TabIndex = 0;
            this.textBoxSalary.TextChanged += new System.EventHandler(this.textBoxSalary_TextChanged);
            // 
            // textBoxSalaryNew
            // 
            this.textBoxSalaryNew.Location = new System.Drawing.Point(365, 221);
            this.textBoxSalaryNew.Name = "textBoxSalaryNew";
            this.textBoxSalaryNew.ReadOnly = true;
            this.textBoxSalaryNew.Size = new System.Drawing.Size(243, 20);
            this.textBoxSalaryNew.TabIndex = 1;
            // 
            // labelSalary
            // 
            this.labelSalary.AutoSize = true;
            this.labelSalary.Location = new System.Drawing.Point(186, 99);
            this.labelSalary.Name = "labelSalary";
            this.labelSalary.Size = new System.Drawing.Size(129, 13);
            this.labelSalary.TabIndex = 5;
            this.labelSalary.Text = "Employee\'s Current Salary";
            // 
            // labelSalaryNew
            // 
            this.labelSalaryNew.AutoSize = true;
            this.labelSalaryNew.Location = new System.Drawing.Point(136, 228);
            this.labelSalaryNew.Name = "labelSalaryNew";
            this.labelSalaryNew.Size = new System.Drawing.Size(208, 13);
            this.labelSalaryNew.TabIndex = 6;
            this.labelSalaryNew.Text = "Employee\'s Next Year Salary (4% increase)";
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(614, 362);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(124, 43);
            this.buttonExit.TabIndex = 4;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(73, 362);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(110, 43);
            this.buttonClear.TabIndex = 3;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // ProjectedRaisesGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.labelSalaryNew);
            this.Controls.Add(this.labelSalary);
            this.Controls.Add(this.textBoxSalaryNew);
            this.Controls.Add(this.textBoxSalary);
            this.Controls.Add(this.buttonCalculate);
            this.Name = "ProjectedRaisesGUI";
            this.Text = "ProjectedRaisesGUI";
            
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.TextBox textBoxSalary;
        private System.Windows.Forms.TextBox textBoxSalaryNew;
        private System.Windows.Forms.Label labelSalary;
        private System.Windows.Forms.Label labelSalaryNew;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonClear;
    }
}

