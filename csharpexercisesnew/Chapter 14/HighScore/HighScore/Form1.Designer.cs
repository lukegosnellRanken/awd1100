﻿namespace HighScore
{
    partial class highScore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxChoices = new System.Windows.Forms.GroupBox();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.radioButtonA = new System.Windows.Forms.RadioButton();
            this.radioButtonB = new System.Windows.Forms.RadioButton();
            this.radioButtonC = new System.Windows.Forms.RadioButton();
            this.labelUserChoice = new System.Windows.Forms.Label();
            this.labelHighScore = new System.Windows.Forms.Label();
            this.labelCurrentScore = new System.Windows.Forms.Label();
            this.labelComputerChoice = new System.Windows.Forms.Label();
            this.labelGroupBox = new System.Windows.Forms.Label();
            this.groupBoxChoices.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxChoices
            // 
            this.groupBoxChoices.Controls.Add(this.labelGroupBox);
            this.groupBoxChoices.Controls.Add(this.radioButtonC);
            this.groupBoxChoices.Controls.Add(this.radioButtonB);
            this.groupBoxChoices.Controls.Add(this.radioButtonA);
            this.groupBoxChoices.Location = new System.Drawing.Point(56, 42);
            this.groupBoxChoices.Name = "groupBoxChoices";
            this.groupBoxChoices.Size = new System.Drawing.Size(200, 244);
            this.groupBoxChoices.TabIndex = 0;
            this.groupBoxChoices.TabStop = false;
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Location = new System.Drawing.Point(56, 323);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(200, 70);
            this.buttonSubmit.TabIndex = 1;
            this.buttonSubmit.Text = "Submit Guess";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // radioButtonA
            // 
            this.radioButtonA.AutoSize = true;
            this.radioButtonA.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonA.Location = new System.Drawing.Point(75, 67);
            this.radioButtonA.Name = "radioButtonA";
            this.radioButtonA.Size = new System.Drawing.Size(45, 29);
            this.radioButtonA.TabIndex = 0;
            this.radioButtonA.TabStop = true;
            this.radioButtonA.Text = "A";
            this.radioButtonA.UseVisualStyleBackColor = true;
            // 
            // radioButtonB
            // 
            this.radioButtonB.AutoSize = true;
            this.radioButtonB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonB.Location = new System.Drawing.Point(75, 117);
            this.radioButtonB.Name = "radioButtonB";
            this.radioButtonB.Size = new System.Drawing.Size(45, 29);
            this.radioButtonB.TabIndex = 1;
            this.radioButtonB.TabStop = true;
            this.radioButtonB.Text = "B";
            this.radioButtonB.UseVisualStyleBackColor = true;
            // 
            // radioButtonC
            // 
            this.radioButtonC.AutoSize = true;
            this.radioButtonC.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonC.Location = new System.Drawing.Point(75, 170);
            this.radioButtonC.Name = "radioButtonC";
            this.radioButtonC.Size = new System.Drawing.Size(46, 29);
            this.radioButtonC.TabIndex = 2;
            this.radioButtonC.TabStop = true;
            this.radioButtonC.Text = "C";
            this.radioButtonC.UseVisualStyleBackColor = true;
            // 
            // labelUserChoice
            // 
            this.labelUserChoice.AutoSize = true;
            this.labelUserChoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUserChoice.Location = new System.Drawing.Point(287, 54);
            this.labelUserChoice.Name = "labelUserChoice";
            this.labelUserChoice.Size = new System.Drawing.Size(172, 31);
            this.labelUserChoice.TabIndex = 2;
            this.labelUserChoice.Text = "User Choice:";
            // 
            // labelHighScore
            // 
            this.labelHighScore.AutoSize = true;
            this.labelHighScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHighScore.Location = new System.Drawing.Point(288, 243);
            this.labelHighScore.Name = "labelHighScore";
            this.labelHighScore.Size = new System.Drawing.Size(254, 31);
            this.labelHighScore.TabIndex = 3;
            this.labelHighScore.Text = "Current High Score:";
            // 
            // labelCurrentScore
            // 
            this.labelCurrentScore.AutoSize = true;
            this.labelCurrentScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCurrentScore.Location = new System.Drawing.Point(287, 182);
            this.labelCurrentScore.Name = "labelCurrentScore";
            this.labelCurrentScore.Size = new System.Drawing.Size(255, 31);
            this.labelCurrentScore.TabIndex = 4;
            this.labelCurrentScore.Text = "Your Current Score:";
            // 
            // labelComputerChoice
            // 
            this.labelComputerChoice.AutoSize = true;
            this.labelComputerChoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelComputerChoice.Location = new System.Drawing.Point(287, 117);
            this.labelComputerChoice.Name = "labelComputerChoice";
            this.labelComputerChoice.Size = new System.Drawing.Size(233, 31);
            this.labelComputerChoice.TabIndex = 5;
            this.labelComputerChoice.Text = "Computer Choice:";
            // 
            // labelGroupBox
            // 
            this.labelGroupBox.AutoSize = true;
            this.labelGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGroupBox.Location = new System.Drawing.Point(30, 25);
            this.labelGroupBox.Name = "labelGroupBox";
            this.labelGroupBox.Size = new System.Drawing.Size(143, 18);
            this.labelGroupBox.TabIndex = 3;
            this.labelGroupBox.Text = "Make Your Guess";
            // 
            // highScore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelComputerChoice);
            this.Controls.Add(this.labelCurrentScore);
            this.Controls.Add(this.labelHighScore);
            this.Controls.Add(this.labelUserChoice);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.groupBoxChoices);
            this.Name = "highScore";
            this.Text = "High Score";
            this.Load += new System.EventHandler(this.highScore_Load);
            this.groupBoxChoices.ResumeLayout(false);
            this.groupBoxChoices.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxChoices;
        private System.Windows.Forms.RadioButton radioButtonC;
        private System.Windows.Forms.RadioButton radioButtonB;
        private System.Windows.Forms.RadioButton radioButtonA;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.Label labelUserChoice;
        private System.Windows.Forms.Label labelHighScore;
        private System.Windows.Forms.Label labelCurrentScore;
        private System.Windows.Forms.Label labelComputerChoice;
        private System.Windows.Forms.Label labelGroupBox;
    }
}

