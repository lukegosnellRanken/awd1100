﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ConvertMilesToKilometers
{
    class Program
    {
        const double MILESINAKILO = 0.621371;

        static string userInput = "";
        static double miles = 0.0;

        static void Test(ref double kilometers, miles)
        {
            return miles * MILESINAKILO;

        }
        

        static void Main(string[] args)
        {
            string userInput = "";
            
            WriteLine("Welcome to the Miles to Kilometers Calculator!");
            WriteLine("\nNumber of miles: ");
            userInput = ReadLine();
            miles = Convert.ToDouble(userInput);

            convertToKilo(miles);

            WriteLine("\n" + userInput + " miles is " + kilometers + " kilometers.");
            ReadLine();

        }

        static void convertToKilo(double miles)
        {
            double kilometers = 0;

            kilometers = Test(ref kilometers, miles);


        }
    }
}
