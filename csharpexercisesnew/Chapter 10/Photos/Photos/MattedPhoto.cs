﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Photos
{
    public class MattedPhoto : Photo
    {
        protected Color _color;

        public MattedPhoto(float width, float height, Color color) : base(width, height)
        {
            _color = color;
        }

        public Color Color
        {
            get { return _color; }
            set { _color = value; }
        }

        public override float Price
        {
            get
            {
                return base.Price + 10;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}x{1} MattedPhoto ({2})", _width, _height, _color);
        }
    }
}
