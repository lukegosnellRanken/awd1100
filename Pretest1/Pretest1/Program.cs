﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Pretest1
{
    class Program
    {
        const int ARRAYSIZE = 25;

        static int[] numArray = new int[ARRAYSIZE];
        static int[] origArray = new int[ARRAYSIZE];

        static void Main(string[] args)
        {
            int menuChoice;

            while (1 == 1)
            {
                menuChoice = displayMenu();
                callCorrectMethod(menuChoice);
            }
        }

        static int displayMenu()
        {
            string choiceStr;
            int choice = 0;

            string menuStr = "Enter a 0 to end the program now. \n";
            menuStr += "Enter a 1 to generate a new array. \n";
            menuStr += "Enter a 2 to print out the array. \n";
            menuStr += "Enter a 3 to print out the array in ascending order. \n";
            menuStr += "Enter a 4 to print out the array in descending order. \n";
            menuStr += "Enter a 5 to print out the smallest number. \n";
            menuStr += "Enter a 6 to print out the largest number. \n";
            menuStr += "Enter a 7 to print out the sum of the numbers. \n";
            menuStr += "Enter a 8 to print the average of all numbers.. \n";
            menuStr += "Enter a 0, 1, 2, 3, 4, 5, 6, 7, or 8. \n";

            Console.Clear();
            Write(menuStr);
            choiceStr = ReadLine();

            if (isNumeric(choiceStr))
            {
                choice = Convert.ToInt32(choiceStr);
                if ((choice < 0) || (choice > 8))
                {
                    WriteLine("OOR input");
                    ReadLine();
                    displayMenu();
                }
            }
            else
            {
                WriteLine("Non-Numeric or OOR input. Re-enter");
                ReadLine();
                displayMenu();
            }

            return choice;
        }

        static bool isNumeric(String input)
        {
            int test;
            return int.TryParse(input, out test);
        }

        static void callCorrectMethod(int mc)
        {
            switch (mc)
            {
                case 0:
                    Environment.Exit(0);
                    break;
                case 1:
                    generateNewArray();
                    break;
                case 2:
                    showArrayInOriginalOrder();
                    break;
                case 3:
                    showArrayInAscendingOrder(true);
                    break;
                case 4:
                    showArrayInDescendingOrder(true);
                    break;
                case 5:
                    showArrayInAscendingOrder(false);
                    WriteLine("\n\nSmallest array element: " + numArray[0].ToString());
                    ReadLine();
                    break;
                case 6:
                    showArrayInDescendingOrder(false);
                    WriteLine("\n\nLargest array element: " + numArray[0].ToString());
                    ReadLine();
                    break;
                case 7:
                    sumArrayElements(true);
                    break;
                case 8:
                    sumArrayElements(false);
                    break;
                default:
                    break;
            }
        }

        static void generateNewArray()
        {
            Random rand = new Random();

            for (int lcv = 0; lcv < numArray.Length; ++lcv)
            {
                numArray[lcv] = rand.Next(0, 101);
                origArray[lcv] = numArray[lcv];
            }

            WriteLine("New array has been generated. \n\n");
            ReadLine();
        }

        static void showArrayInOriginalOrder()
        {
            printOutArray(origArray, 1, true);
        }

        static void showArrayInAscendingOrder(bool flag)
        {
            Array.Sort(numArray);

            if (flag)
            {
                printOutArray(numArray, 2, flag);
            }
        }

        static void showArrayInDescendingOrder(bool flag)
        {
            Array.Sort(numArray);
            Array.Reverse(numArray);

            if (flag)
            {
                printOutArray(numArray, 3, flag);
            }
        }

        static void printOutArray(int[] theArray, int selection, bool flag)
        {
            string sel = "";

            if (flag)
            {
                switch (selection)
                {
                    case 1:
                        sel = "original order";
                        break;
                    case 2:
                        sel = "Ascending order";
                        break;
                    case 3:
                        sel = "descending order";
                        break;
                    default:
                        break;
                }

                WriteLine("\n\nNow Printing Out the array in " + sel);
                for (int lcv = 0; lcv < ARRAYSIZE; ++lcv)
                {
                    Write(theArray[lcv] + "  ");
                }

                WriteLine("");
                ReadLine();
            }
        }

        static void sumArrayElements(bool flag)
        {
            int sum = 0;
            double avg = 0.0;

            for (int lcv = 0; lcv < ARRAYSIZE; ++lcv)
            {
                sum += numArray[lcv];
            }

            if (flag)
            {
                WriteLine("\n\nThe sum of all array elements was: " + sum.ToString());
            }
            else
            {
                avg = (double)sum / ARRAYSIZE;
                WriteLine("\n\nThe Avg of all array elements was: " + avg.ToString("f2"));
            }

            ReadLine();
        }
    }
}
