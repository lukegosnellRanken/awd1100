﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 *  Create a C# Console Random Number Guessing Game 
 *  (save as GuessingGame with no blank spaces).
 *  
 *  Generate a random number between 1 and 100 (use 
 *  constants).  Ask the user to guess what the number 
 *  is and check the user's guess. (10 points)
 *  
 *  Handle non-numeric input and out-of-range input by 
 *  showing the player a "MessageBox" but do not count 
 *  any of these as an actual guess. (10 points)
 *  
 *  If the user's guess is higher than the random number, 
 *  the program should display "Too high, try again." (5
 *  points).
 *  
 *  If the user's guess is lower than the random number, 
 *  the program should display "Too low, try again."  (5 
 *  points)
 *  
 *  When the user correctly guesses the random number, the 
 *  program should display the number of guesses and the 
 *  random number as shown below and also ask the user if 
 *  s/he wants to play again. (10 points)
 *  
 *  The remaining 10 points will be awarded based on how 
 *  well the instructions on these two pages are followed; 
 *  the use of good variable and constant names, no 
 *  unnecessary code, and sound program logic. (10 points)
 */
namespace GuessingGame
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
