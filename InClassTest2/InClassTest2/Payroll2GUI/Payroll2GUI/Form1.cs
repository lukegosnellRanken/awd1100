﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 *  In your previous test, you wrote a C# program
 *  named Payroll that calculated gross pay and 
 *  printed out the employee information for two 
 *  employees.
 *  
 *  Create Payroll2GUI, using the same variables 
 *  for each employee, i.e.: (5 points)
 *  
 *  a)	firstName (string) b) lastName (string) 
 *  c) hoursWorked (double) d) hourlyRate (double)
 *  
 *  Allow for as many employees as desired.  Create 
 *  the following constants, each type double: (5 points)
 *  
 *  •	MINHOURS = 0.0;  MAXHOURS = 84.0; MINRATE = 0.0;
 *  •	MAXRATE = 250.0; MAXNONOT = 40;   OTRATE = 1.5;
 *  
 *  For each employee who worked <= 40 hours, calculate 
 *  said employee's grossPay as hoursWorked * hourlyRate.
 *  (5 points)
 *  
 *  For each employee who worked > 40 hours, calculate said 
 *  employee's grossPay as hoursWorked * hourlyRate for the
 *  first 40 hours and hoursWorked * hourlyRate * OTRATE for
 *  all hours worked > 40. (5 points)
 *  
 *  Keep a running total (accumulator) totPayroll, which holds 
 *  the sum of all gross pays.  (10 points)
 *  
 *  Show/program the same info as in the GUI on the next page:
 *  (10 points)
 *  
 *  The remaining 10 points will be awarded based on how well 
 *  the instructions on these two pages are followed; the use 
 *  of good variable and constant names, no unnecessary code, 
 *  and sound program logic. (10 points) 
 */

namespace Payroll2GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
    }
}
