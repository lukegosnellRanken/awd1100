﻿using System;
using static System.Console;

namespace Students02
{
    class Program
    {
        const int NUMSTUDS = 5;

        static string[] firstName   = new string[NUMSTUDS];
        static string[]  lastName   = new string[NUMSTUDS];
        static int[] numberGrade    = new int[NUMSTUDS];
        static char[] letterGrade   = new char[NUMSTUDS];
        static int highGrade        = -0;
        static int lowGrade         = 101;
        static int totalPts         = 0;
        static int totGrades        = 0;
        static double avgGrade      = 0.0;

        static void Main(string[] args)
        {
            for (int lcv = 0; lcv < NUMSTUDS; ++lcv)
            {
                Console.Clear();
                setFirstName(lcv);
                setLastName(lcv);
                setNumberGrade(lcv);
                setLetterGrade(lcv);
                displayStats(lcv);
            }

            Console.Clear();
            displayHighGrade();
            displayLowGrade();
            displayAvgGrade();
            ReadLine();
        }

        static void setFirstName(int lcv)
        {
            Write("Enter student " + (lcv + 1) + " first name: ");
            firstName[lcv] = ReadLine();
        }

        static void setLastName(int lcv)
        {
            Write("Enter student " + (lcv + 1) + "  last name: ");
            lastName[lcv] = ReadLine();
        }

        static void setNumberGrade(int lcv)
        {
            Random random = new Random();
            numberGrade[lcv] = random.Next(0, 101);
            ++totGrades;

            totalPts += numberGrade[lcv];

            highGradeCheck(lcv);
            lowGradeCheck(lcv);
        }

        static void highGradeCheck(int lcv)
        {
            if (numberGrade[lcv] > highGrade)
            {
                highGrade = numberGrade[lcv];
            }
        }

        static void lowGradeCheck(int lcv)
        {
            if (numberGrade[lcv] < lowGrade)
            {
                lowGrade = numberGrade[lcv];
            }
        }

        static void setLetterGrade(int lcv)
        {
            if (numberGrade[lcv] >= 90)
            {
                letterGrade[lcv] = 'A';
            }
            else if (numberGrade[lcv] >= 80)
            {
                letterGrade[lcv] = 'B';
            }
            else if (numberGrade[lcv] >= 70)
            {
                letterGrade[lcv] = 'C';
            }
            else if (numberGrade[lcv] >= 60)
            {
                letterGrade[lcv] = 'D';
            }
            else
            {
                letterGrade[lcv] = 'F';
            }
        }

        static void displayStats(int lcv)
        {
            WriteLine("First Name: " + firstName[lcv]);
            WriteLine("Last Name: " + lastName[lcv]);
            WriteLine("The number grade: " + numberGrade[lcv]);
            WriteLine("The letter grade was: " + letterGrade[lcv]);
            ReadLine();
        }

        static void displayHighGrade()
        {
            WriteLine("The High Grade Was: " + highGrade);
        }

        static void displayLowGrade()
        {
            WriteLine("The Low Grade Was: " + lowGrade);
        }

        static void displayAvgGrade()
        {
            avgGrade = (double)totalPts / totGrades;
            WriteLine("The total number of student grades: " + totGrades);
            WriteLine("The average grade was: " + avgGrade.ToString("f2"));
        }
    }
}
