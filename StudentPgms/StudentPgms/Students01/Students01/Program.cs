﻿using System;
using static System.Console;

namespace Students01
{
    class Program
    {
        static string firstName =   "";
        static string lastName  =   "";
        static int numberGrade  =   0;
        static char letterGrade =   ' ';
        static int highGrade    =  -0;
        static int lowGrade     = 101;
        static int totalPts     =   0;
        static int totGrades    =   0;
        static double avgGrade  = 0.0;

        static void Main(string[] args)
        {
            int numStudents     = 0;
            const int NUMSTUDS  = 5;

            while (numStudents < NUMSTUDS)
            {
                Console.Clear();
                setFirstName();
                setLastName();
                setNumberGrade();
                setLetterGrade();
                displayStats();
                ++numStudents;
            }

            Console.Clear();
            displayHighGrade();
            displayLowGrade();
            displayAvgGrade();
            ReadLine();
        }

        static void setFirstName()
        {
            Write("Enter first name: ");
            firstName = ReadLine();
        }

        static void setLastName()
        {
            Write("Enter last name: ");
            lastName = ReadLine();
        }

        static void setNumberGrade()
        {
            Random random = new Random();
            numberGrade = random.Next(0, 101);
            ++totGrades;

            totalPts += numberGrade;

            highGradeCheck();
            lowGradeCheck();
        }

        static void highGradeCheck()
        {
            if (numberGrade > highGrade)
            {
                highGrade = numberGrade;
            }
        }

        static void lowGradeCheck()
        {
            if (numberGrade < lowGrade)
            {
                lowGrade = numberGrade;
            }
        }

        static void setLetterGrade()
        {
            if (numberGrade >= 90)
            {
                letterGrade = 'A';
            }
            else if (numberGrade >= 80)
            {
                letterGrade = 'B';
            }
            else if (numberGrade >= 70)
            {
                letterGrade = 'C';
            }
            else if (numberGrade >= 60)
            {
                letterGrade = 'D';
            }
            else
            {
                letterGrade = 'F';
            }
        }

        static void displayStats()
        {
            WriteLine("First Name: " + firstName);
            WriteLine("Last Name: " + lastName);
            WriteLine("The number grade: " + numberGrade);
            WriteLine("The letter grade was: " + letterGrade);
            ReadLine();
        }

        static void displayHighGrade()
        {
            WriteLine("The High Grade Was: " + highGrade);
        }

        static void displayLowGrade()
        {
            WriteLine("The Low Grade Was: " + lowGrade);
        }

        static void displayAvgGrade()
        {
            avgGrade = (double)totalPts / totGrades;
            WriteLine("The total number of student grades: " + totGrades);
            WriteLine("The average grade was: " + avgGrade.ToString("f2"));
        }
    }
}
